'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Messenger from '../components/messenger';
import * as messagesAction from '../actions/messagesActions';
import { connect } from 'react-redux';

// @connect(state => ({
//   state: state.counter
// }))
class MessengerApp extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    }

    render() {

        //todo сделай Map to Array или придумай как messages map()
        const { state, actions } = this.props;
        let messageMap = state.get("messages").toArray();
        messageMap.forEach(function(item){
            item.toArray();
        });
        debugger
        return (
                <Messenger messages={messageMap} {...actions} />
        );
    }
}

export default connect(state => ({
    state: state.messenger
}),
    (dispatch) => ({
        actions: bindActionCreators(messagesAction, dispatch)
    })
)(MessengerApp);