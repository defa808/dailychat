'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import SignIn from '../../components/authorization/signIn';
import * as authorizationAction from '../../actions/authorizationActions';
import { connect } from 'react-redux';

class SignInContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { state, actions } = this.props;
        return (
                <SignIn authorization={state.authorization} {...actions} />
        );
    }
}

export default connect(state => ({
        state: state.signInReducer
    }),
    (dispatch) => ({
        actions: bindActionCreators(authorizationAction, dispatch)
    })
)(SignInContainer);