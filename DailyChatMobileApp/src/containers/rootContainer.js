'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import * as authorizationAction from '../actions/authorizationActions';
import { connect } from 'react-redux';
import SignInContainer from './authorization/signInContainer';
import { Provider } from 'react-redux';
import MessengerApp from './messengerApp';

class RootContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { state, actions } = this.props;
        debugger
        if (state.get("authorization") && state.get("authorization").apiToken != "")
            return (
                <Provider store={this.props.store}>
                    <MessengerApp messages={state.get("messages")} />
                </Provider>
            )
        else {
            return (
                <Provider store={this.props.store}>
                    <SignInContainer authorization={state.get("authorization")} {...actions} />
                </Provider>)
        }
    }
}

export default connect(state => ({
    state: state.signInReducer
}),
    (dispatch) => ({
        actions: bindActionCreators(authorizationAction, dispatch)
    })
)(RootContainer);
