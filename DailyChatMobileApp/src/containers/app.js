import React, {Component} from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { reducer as form } from 'redux-form/immutable' // <--- immutable import
import * as reducers from '../reducers';
import MessengerApp from './messengerApp';
import CounterApp from './counterApp';
import SignInContainer from './authorization/signInContainer'
import RootContainer from './rootContainer'


const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);

export default class App extends Component {
    render() {
        console.log(store);
        console.log('----------');
        return (
            <Provider store={store}>
                <RootContainer store={store}/>
            </Provider>
        );
    }
}