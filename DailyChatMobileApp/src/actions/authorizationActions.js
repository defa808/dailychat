import * as types from './actionTypes';
import AuthorizationService from "../services/authorizationService";

export function SignInAttempt(values) {
    return (dispatch, getState) => {
        AuthorizationService.SignInAttempt(values)
            .then((data) => {
                dispatch(SignInSuccess(data));
            }).catch((error) => {
                dispatch(SignInError(error));
            }
        );
        return {
            type: types.SignInAttempt,
            authorization: {
                telephone: values,
                apiToken: ""
            }
        };
    }
}

export function SignInSuccess(token) {

    return {
        type: types.SignInSuccess,
        authorization: {
            apiToken: token
        }
    };
}

export function SignInError(telephone) {

    return {
        type: types.SignInSuccess,
        authorization: {
            telephone: telephone,
            apiToken: ""
        }
    };
}


export function signUp() {
    return {
        type: types.SignUp
    };
}

