import * as types from './actionTypes';
import MessageService from "../services/messageService";
import AuthorizationService from "../services/authorizationService";
import {SignInError, SignInSuccess} from "./authorizationActions";

export function addMessageAttempt(message, apiToken) {
    return (dispatch, getState) => {
        MessageService.sendMessage(message, apiToken)
            .then((responce) => {
                dispatch(addMessageSuccess(message));
            })
            .catch((e) => {

            });
        return {
            type: types.AddMessageAttempt,
            message: message
        };
    }
}

export function addMessageSuccess(message) {
    return {
        type: types.AddMessageSuccess,
        message: {author:"No way", text:message.message}
    };
}


export function addMessageError(message) {
    return {
        type: types.AddMessageError,
        message: message
    };
}

export function removeMessage() {
    return {
        type: types.removeMessage
    };
}

export function editMessage() {
    return {
        type: types.editMessage
    };
}