import * as types from '../actions/actionTypes';
import Immutable from "immutable";


const initialState = new Immutable.fromJS({
    messages: [{
        author: "Alex",
        text: "Hello World"
    }]
});


export default function messenger(state = initialState, action = {}) {
    debugger
    switch (action.type) {
        case types.AddMessageAttempt: {
            return {
                ...state,
            };
        }

        case types.AddMessageSuccess: {
            debugger
            return state.updateIn(["messages"], arr => arr.push(action.message));
        }

        case types.EditMessage:
            return {
                ...state
            };
        case types.DeleteMessage:
            state.messages.pop();
            return {
                ...state,
            };
        default:
            return state;
    }
}