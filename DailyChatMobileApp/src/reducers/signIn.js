import * as types from '../actions/actionTypes';
import { RandomToken } from 'random-token'
import Immutable from 'immutable';

const initialState = new Immutable.Map({
    authorization: {
        telephone: "",
        apiToken: ""
    },
});


export default function signInReducer(state = initialState, action = {}) {
    debugger
    switch (action.type) {
        case types.SignInSuccess:
        case types.SignUpSuccess: {
            return state.setIn(["authorization","apiToken"], action.authorization.apiToken);
        }

        case types.SignInAttempt:
        case types.SignUpAttempt: {
            return state.setIn(["authorization","telephone"], action.authorization.telephone);
        }

        case types.SignOutAttempt:
            //rewrite
            state.authorization.apiToken = "";
            return {
                ...state,
            };

        default:
            return state;
    }
}

