import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Message from './message/message'
import InputMessage from "./message/inputMessage";
const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    }
});

export default class Messenger extends Component {
    constructor(props) {
        super(props);
    }


    render() {
        const { messages, addMessageAttempt, editMessage, deleteMessage } = this.props;
        debugger
        return (
            <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
                {messages.map((x) => (<Message id={x.id} key={JSON.stringify(x)} model={x} />))}

                <InputMessage sendMessage={addMessageAttempt}/>
            </View>
        );
    }
}