import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    },
    main: {
        flex: 1,
        justifyContent: 'space-between',
    }
});

export default class Message extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {model} = this.props;
        return (
            <View style={styles.main}>
                <Text>{model.author}</Text>
                <Text>{model.text}</Text>
            </View>
        );
    }
}