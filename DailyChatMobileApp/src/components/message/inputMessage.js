import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TextInput, Button} from 'react-native';

const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    },
    main: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',

    }
});

export default class InputMessage extends Component {
    constructor(props) {
        super(props);
        this.message = "";
    }

    sender = () => {
        const sendMessage = this.props.sendMessage;

        debugger
        if (this.message !== "")
            sendMessage({message:this.message});

    };

    render() {
        return (
            <View style={styles.main}>
                <TextInput onChangeText={(value) => this.message = value}/>
                <Button title="Submit Button" onPress={this.sender}/>
            </View>
        );
    }
}