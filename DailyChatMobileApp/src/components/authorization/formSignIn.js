import React from 'react';
import { TextInput, View, Text, ScrollView, StyleSheet, TouchableOpacity, Button } from 'react-native';
import { Field, reduxForm } from "redux-form";

const Form = props => {

  //TODO take old data from authorization
  const { SignInAttempt } = props;
  let phone = "";

  const singInAction = function () {
    SignInAttempt(phone);
  };

  //TODO validation and parsing telephone
  return (
    <ScrollView keyboardShouldPersistTaps={'handled'}>
      <Text>Your Phone:</Text>
      <TextInput onChangeText={(value) => phone = value} />
      <Button title="Button Submit" onPress={singInAction} />
    </ScrollView>
  )
};


const styles = StyleSheet.create({

  container: {}
});


export default reduxForm({ form: 'signOut' })(Form);