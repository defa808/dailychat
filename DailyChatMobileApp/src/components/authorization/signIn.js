import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {TextField} from 'react-native-material-textfield';
import {RaisedTextButton} from 'react-native-material-buttons';
import {TextInput} from 'react-native'
import FormSignIn from './formSignIn';


const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    },
    main: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',

    }
});

export default class SignIn extends Component {
    constructor(props) {
        super(props);
    }

    submit(values) {
        console.log(values);
    }

    handleSubmit(values) {
        debugger;
        console.log("wwww");
        console.log(values);
    }

    render() {
        const {authorization, SignInAttempt} = this.props;
        return (
            <View style={styles.main}>
                <FormSignIn SignInAttempt={SignInAttempt} authorization={authorization} />
            </View>
        );
    }
}

