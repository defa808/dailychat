//some service for api
import * as ApiConstant from '../constants/apiConstant'
import * as AuthorizationActions from "../actions/authorizationActions";

export default class MessageService {


    static sendMessage(message) {
        debugger
        return fetch(ApiConstant.ServerIp + ApiConstant.SendMessage, {
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(message)
        });
    }

}