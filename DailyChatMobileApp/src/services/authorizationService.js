//some service for api
import * as ApiConstant from '../constants/apiConstant'
import * as AuthorizationActions from "../actions/authorizationActions";

export default class AuthorizationService {


    static SignInAttempt(values) {

        return fetch(ApiConstant.ServerIp + ApiConstant.SignIn, {
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST",
            body: "{value:'" + values + "'}"
        }).then((response) => {
            if (response.status >= 200 && response.status < 300) {
                console.log(response);
               return  response.json();
            } else {
                const error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        }).then((data ) =>  {
            console.log(data);
            return data;
        });
    }

}