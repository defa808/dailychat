﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyChatApiV5.Models
{
    public class Channel
    {
        public Guid Id { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        
        public DateTime? TimeStart { get; set; }
        public DateTime? TimeEnd { get; set; }
        public TimeSpan? AllTime { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual ICollection<ApplicationUser> ActiveUsers { get; set; }


        public void OpenChannel()
        {
            TimeStart = DateTime.Now;
            TimeEnd = DateTime.Now.AddDays(1);
        }
        public bool TryCloseChannel()
        {
            //TimeSpan day = DateTime.Now.Date - TimeStart.Date.AddMinutes(1);
            //if (day.Days >= 1 )
            //{

            //}
            return true;
        }
    }

}
