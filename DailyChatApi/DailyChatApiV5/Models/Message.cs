﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyChatApiV5.Models
{
    public class Message
    {
        public Guid Id { get; set; }
        public ApplicationUser UserSender { get; set; }
        public ApplicationUser UserRecipient { get; set; }
        public string TextMessage { get; set; }
        public bool IsRead { get; set; }
        public bool IsEdited { get; set; }
        public virtual ICollection<Message> ForwardMessages { get; set; }
        public virtual ICollection<Message> BackwardsMessages { get; set; }

        public DateTime? TimeReceipt { get; set; }
        public DateTime? TimeSend{ get; set; }
        public Channel Channel { get; set; }


        public Message()
        {
            ForwardMessages = new HashSet<Message>();
        }
    }
}
