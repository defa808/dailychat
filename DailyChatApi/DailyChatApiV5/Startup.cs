﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DailyChatApiV5.Startup))]
namespace DailyChatApiV5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
